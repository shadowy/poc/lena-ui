import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { NgxAuthFirebaseUIModule } from 'ngx-auth-firebaseui';
import { DefaultPageComponent } from './default-page/default-page.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { MainPageComponent } from './main-page/main-page.component';
import { ToolbarComponent } from './main-page/toolbar/toolbar.component';
import { LanguageSelectorComponent } from './main-page/toolbar/language-selector/language-selector.component';
import { UserInfoComponent } from './main-page/toolbar/user-info/user-info.component';
import { NavigationComponent } from './main-page/navigation/navigation.component';



@NgModule({
  declarations: [DefaultPageComponent, LoginPageComponent, MainPageComponent, ToolbarComponent, LanguageSelectorComponent, UserInfoComponent, NavigationComponent],
  imports: [
    CommonModule,
    MatSidenavModule,
    MatListModule,
    MatIconModule,
    RouterModule,
    MatButtonModule,
    MatToolbarModule,
    MatMenuModule,
    TranslateModule,
    NgxAuthFirebaseUIModule
  ],
  exports: [DefaultPageComponent, LoginPageComponent, MainPageComponent]
})
export class SecurityUIModule { }
