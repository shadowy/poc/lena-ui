import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { SecurityState, SecurityUserLogout, UserInfo } from '@smartsales/security';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ToolbarComponent {
  @Select(SecurityState.Version) version$: Observable<string>;
  @Select(SecurityState.CurrentUserInfo) currentUser$: Observable<UserInfo>;

  constructor(private store: Store, private router: Router, private auth: AngularFireAuth) {}

  setTheme(theme: string) {
    (window as any).setTheme(theme);
  }

  signOut() {
    this.auth.signOut().then(() => {
      this.store.dispatch(new SecurityUserLogout());
      return this.router.navigate(['/login']);
    });
  }
}
