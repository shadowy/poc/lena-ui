import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewRef } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { flagLang, StorageService } from '@smartsales/security';

@Component({
  selector: 'app-language-selector',
  templateUrl: './language-selector.component.html',
  styleUrls: ['./language-selector.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LanguageSelectorComponent implements OnInit{
  currentLang: string;

  constructor(private translate: TranslateService, private cdRef: ChangeDetectorRef, private storage: StorageService) {}

  ngOnInit() {
    this.translate.onLangChange.subscribe(lang => {
      this.currentLang = 'flag-icon-' + flagLang[lang.lang];
      this.detectChanges();
    });
  }

  setLang(lang: string) {
    this.storage.setLanguage(lang);
    this.translate.use(lang);
  }

  private detectChanges() {
    if (!(this.cdRef as ViewRef).destroyed) {
      this.cdRef.detectChanges();
    }
  }
}
