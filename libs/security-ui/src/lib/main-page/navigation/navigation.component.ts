import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { NavigationItem } from '@smartsales/security';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NavigationComponent {
  @Input() list: NavigationItem[] = [];
}
