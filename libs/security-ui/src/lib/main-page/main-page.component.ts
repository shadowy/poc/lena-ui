import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Store } from '@ngxs/store';
import { NavigationItem, NavigationState, SecurityState, UserInfo } from '@smartsales/security';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MainPageComponent {
  navigation: NavigationItem[] = [];
  longMenu = false;
  isVisible  = false;

  constructor(store: Store) {
    store.select<UserInfo>(SecurityState.User).subscribe(user => {
      this.isVisible = !!user;
    });
    store.select<NavigationItem[]>(NavigationState.getList).subscribe(list => {
      this.navigation = list;
    });
  }

  toggleMenu() {
    this.longMenu = !this.longMenu;
  }
}
