import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngxs/store';
import { SecurityState, UserInfo } from '@smartsales/security';

@Component({
  selector: 'app-default-page',
  templateUrl: './default-page.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DefaultPageComponent implements OnInit {

  constructor(private store: Store, private router: Router) { }

  ngOnInit() {
    const user = this.store.selectSnapshot<UserInfo>(SecurityState.User);
    if (!user) {
      return;
    }
    return this.router.navigate(['/company/']);
  }

}
