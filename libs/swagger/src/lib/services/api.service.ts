/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { BaseService } from '../base-service';
import { ApiConfiguration } from '../api-configuration';
import { StrictHttpResponse } from '../strict-http-response';
import { RequestBuilder } from '../request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

import { Company } from '../models/company';
import { CreateManyCompanyDto } from '../models/create-many-company-dto';
import { GetManyCompanyResponseDto } from '../models/get-many-company-response-dto';

@Injectable({
  providedIn: 'root',
})
export class ApiService extends BaseService {
  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation getOneBaseCompanyControllerCompany
   */
  static readonly GetOneBaseCompanyControllerCompanyPath = '/company/{id}';

  /**
   * Retrieve one Company.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getOneBaseCompanyControllerCompany()` instead.
   *
   * This method doesn't expect any request body.
   */
  getOneBaseCompanyControllerCompany$Response(params: {
    id: number;

    /**
     * Selects resource fields. &lt;a href&#x3D;&quot;https://github.com/nestjsx/crud/wiki/Requests#select&quot; target&#x3D;&quot;_blank&quot;&gt;Docs&lt;/a&gt;
     */
    fields?: Array<string>;

    /**
     * Adds relational resources. &lt;a href&#x3D;&quot;https://github.com/nestjsx/crud/wiki/Requests#join&quot; target&#x3D;&quot;_blank&quot;&gt;Docs&lt;/a&gt;
     */
    join?: Array<string>;

    /**
     * Reset cache (if was enabled). &lt;a href&#x3D;&quot;https://github.com/nestjsx/crud/wiki/Requests#cache&quot; target&#x3D;&quot;_blank&quot;&gt;Docs&lt;/a&gt;
     */
    cache?: number;

  }): Observable<StrictHttpResponse<Company>> {

    const rb = new RequestBuilder(this.rootUrl, ApiService.GetOneBaseCompanyControllerCompanyPath, 'get');
    if (params) {

      rb.path('id', params.id, {});
      rb.query('fields', params.fields, {"style":"form"});
      rb.query('join', params.join, {"style":"form","explode":true});
      rb.query('cache', params.cache, {});

    }
    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Company>;
      })
    );
  }

  /**
   * Retrieve one Company.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `getOneBaseCompanyControllerCompany$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getOneBaseCompanyControllerCompany(params: {
    id: number;

    /**
     * Selects resource fields. &lt;a href&#x3D;&quot;https://github.com/nestjsx/crud/wiki/Requests#select&quot; target&#x3D;&quot;_blank&quot;&gt;Docs&lt;/a&gt;
     */
    fields?: Array<string>;

    /**
     * Adds relational resources. &lt;a href&#x3D;&quot;https://github.com/nestjsx/crud/wiki/Requests#join&quot; target&#x3D;&quot;_blank&quot;&gt;Docs&lt;/a&gt;
     */
    join?: Array<string>;

    /**
     * Reset cache (if was enabled). &lt;a href&#x3D;&quot;https://github.com/nestjsx/crud/wiki/Requests#cache&quot; target&#x3D;&quot;_blank&quot;&gt;Docs&lt;/a&gt;
     */
    cache?: number;

  }): Observable<Company> {

    return this.getOneBaseCompanyControllerCompany$Response(params).pipe(
      map((r: StrictHttpResponse<Company>) => r.body as Company)
    );
  }

  /**
   * Path part for operation replaceOneBaseCompanyControllerCompany
   */
  static readonly ReplaceOneBaseCompanyControllerCompanyPath = '/company/{id}';

  /**
   * Replace one Company.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `replaceOneBaseCompanyControllerCompany()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  replaceOneBaseCompanyControllerCompany$Response(params: {
    id: number;
      body: Company
  }): Observable<StrictHttpResponse<Company>> {

    const rb = new RequestBuilder(this.rootUrl, ApiService.ReplaceOneBaseCompanyControllerCompanyPath, 'put');
    if (params) {

      rb.path('id', params.id, {});

      rb.body(params.body, 'application/json');
    }
    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Company>;
      })
    );
  }

  /**
   * Replace one Company.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `replaceOneBaseCompanyControllerCompany$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  replaceOneBaseCompanyControllerCompany(params: {
    id: number;
      body: Company
  }): Observable<Company> {

    return this.replaceOneBaseCompanyControllerCompany$Response(params).pipe(
      map((r: StrictHttpResponse<Company>) => r.body as Company)
    );
  }

  /**
   * Path part for operation deleteOneBaseCompanyControllerCompany
   */
  static readonly DeleteOneBaseCompanyControllerCompanyPath = '/company/{id}';

  /**
   * Delete one Company.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `deleteOneBaseCompanyControllerCompany()` instead.
   *
   * This method doesn't expect any request body.
   */
  deleteOneBaseCompanyControllerCompany$Response(params: {
    id: number;

  }): Observable<StrictHttpResponse<void>> {

    const rb = new RequestBuilder(this.rootUrl, ApiService.DeleteOneBaseCompanyControllerCompanyPath, 'delete');
    if (params) {

      rb.path('id', params.id, {});

    }
    return this.http.request(rb.build({
      responseType: 'text',
      accept: '*/*'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return (r as HttpResponse<any>).clone({ body: undefined }) as StrictHttpResponse<void>;
      })
    );
  }

  /**
   * Delete one Company.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `deleteOneBaseCompanyControllerCompany$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  deleteOneBaseCompanyControllerCompany(params: {
    id: number;

  }): Observable<void> {

    return this.deleteOneBaseCompanyControllerCompany$Response(params).pipe(
      map((r: StrictHttpResponse<void>) => r.body as void)
    );
  }

  /**
   * Path part for operation updateOneBaseCompanyControllerCompany
   */
  static readonly UpdateOneBaseCompanyControllerCompanyPath = '/company/{id}';

  /**
   * Update one Company.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `updateOneBaseCompanyControllerCompany()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  updateOneBaseCompanyControllerCompany$Response(params: {
    id: number;
      body: Company
  }): Observable<StrictHttpResponse<Company>> {

    const rb = new RequestBuilder(this.rootUrl, ApiService.UpdateOneBaseCompanyControllerCompanyPath, 'patch');
    if (params) {

      rb.path('id', params.id, {});

      rb.body(params.body, 'application/json');
    }
    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Company>;
      })
    );
  }

  /**
   * Update one Company.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `updateOneBaseCompanyControllerCompany$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  updateOneBaseCompanyControllerCompany(params: {
    id: number;
      body: Company
  }): Observable<Company> {

    return this.updateOneBaseCompanyControllerCompany$Response(params).pipe(
      map((r: StrictHttpResponse<Company>) => r.body as Company)
    );
  }

  /**
   * Path part for operation getManyBaseCompanyControllerCompany
   */
  static readonly GetManyBaseCompanyControllerCompanyPath = '/company';

  /**
   * Retrieve many Company.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `getManyBaseCompanyControllerCompany()` instead.
   *
   * This method doesn't expect any request body.
   */
  getManyBaseCompanyControllerCompany$Response(params?: {

    /**
     * Selects resource fields. &lt;a href&#x3D;&quot;https://github.com/nestjsx/crud/wiki/Requests#select&quot; target&#x3D;&quot;_blank&quot;&gt;Docs&lt;/a&gt;
     */
    fields?: Array<string>;

    /**
     * Adds search condition. &lt;a href&#x3D;&quot;https://github.com/nestjsx/crud/wiki/Requests#search&quot; target&#x3D;&quot;_blank&quot;&gt;Docs&lt;/a&gt;
     */
    's'?: string;

    /**
     * Adds filter condition. &lt;a href&#x3D;&quot;https://github.com/nestjsx/crud/wiki/Requests#filter&quot; target&#x3D;&quot;_blank&quot;&gt;Docs&lt;/a&gt;
     */
    filter?: Array<string>;

    /**
     * Adds OR condition. &lt;a href&#x3D;&quot;https://github.com/nestjsx/crud/wiki/Requests#or&quot; target&#x3D;&quot;_blank&quot;&gt;Docs&lt;/a&gt;
     */
    or?: Array<string>;

    /**
     * Adds sort by field. &lt;a href&#x3D;&quot;https://github.com/nestjsx/crud/wiki/Requests#sort&quot; target&#x3D;&quot;_blank&quot;&gt;Docs&lt;/a&gt;
     */
    sort?: Array<string>;

    /**
     * Adds relational resources. &lt;a href&#x3D;&quot;https://github.com/nestjsx/crud/wiki/Requests#join&quot; target&#x3D;&quot;_blank&quot;&gt;Docs&lt;/a&gt;
     */
    join?: Array<string>;

    /**
     * Limit amount of resources. &lt;a href&#x3D;&quot;https://github.com/nestjsx/crud/wiki/Requests#limit&quot; target&#x3D;&quot;_blank&quot;&gt;Docs&lt;/a&gt;
     */
    limit?: number;

    /**
     * Offset amount of resources. &lt;a href&#x3D;&quot;https://github.com/nestjsx/crud/wiki/Requests#offset&quot; target&#x3D;&quot;_blank&quot;&gt;Docs&lt;/a&gt;
     */
    offset?: number;

    /**
     * Page portion of resources. &lt;a href&#x3D;&quot;https://github.com/nestjsx/crud/wiki/Requests#page&quot; target&#x3D;&quot;_blank&quot;&gt;Docs&lt;/a&gt;
     */
    page?: number;

    /**
     * Reset cache (if was enabled). &lt;a href&#x3D;&quot;https://github.com/nestjsx/crud/wiki/Requests#cache&quot; target&#x3D;&quot;_blank&quot;&gt;Docs&lt;/a&gt;
     */
    cache?: number;

  }): Observable<StrictHttpResponse<GetManyCompanyResponseDto | Array<Company>>> {

    const rb = new RequestBuilder(this.rootUrl, ApiService.GetManyBaseCompanyControllerCompanyPath, 'get');
    if (params) {

      rb.query('fields', params.fields, {"style":"form"});
      rb.query('s', params['s'], {});
      rb.query('filter', params.filter, {"style":"form","explode":true});
      rb.query('or', params.or, {"style":"form","explode":true});
      rb.query('sort', params.sort, {"style":"form","explode":true});
      rb.query('join', params.join, {"style":"form","explode":true});
      rb.query('limit', params.limit, {});
      rb.query('offset', params.offset, {});
      rb.query('page', params.page, {});
      rb.query('cache', params.cache, {});

    }
    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<GetManyCompanyResponseDto | Array<Company>>;
      })
    );
  }

  /**
   * Retrieve many Company.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `getManyBaseCompanyControllerCompany$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  getManyBaseCompanyControllerCompany(params?: {

    /**
     * Selects resource fields. &lt;a href&#x3D;&quot;https://github.com/nestjsx/crud/wiki/Requests#select&quot; target&#x3D;&quot;_blank&quot;&gt;Docs&lt;/a&gt;
     */
    fields?: Array<string>;

    /**
     * Adds search condition. &lt;a href&#x3D;&quot;https://github.com/nestjsx/crud/wiki/Requests#search&quot; target&#x3D;&quot;_blank&quot;&gt;Docs&lt;/a&gt;
     */
    's'?: string;

    /**
     * Adds filter condition. &lt;a href&#x3D;&quot;https://github.com/nestjsx/crud/wiki/Requests#filter&quot; target&#x3D;&quot;_blank&quot;&gt;Docs&lt;/a&gt;
     */
    filter?: Array<string>;

    /**
     * Adds OR condition. &lt;a href&#x3D;&quot;https://github.com/nestjsx/crud/wiki/Requests#or&quot; target&#x3D;&quot;_blank&quot;&gt;Docs&lt;/a&gt;
     */
    or?: Array<string>;

    /**
     * Adds sort by field. &lt;a href&#x3D;&quot;https://github.com/nestjsx/crud/wiki/Requests#sort&quot; target&#x3D;&quot;_blank&quot;&gt;Docs&lt;/a&gt;
     */
    sort?: Array<string>;

    /**
     * Adds relational resources. &lt;a href&#x3D;&quot;https://github.com/nestjsx/crud/wiki/Requests#join&quot; target&#x3D;&quot;_blank&quot;&gt;Docs&lt;/a&gt;
     */
    join?: Array<string>;

    /**
     * Limit amount of resources. &lt;a href&#x3D;&quot;https://github.com/nestjsx/crud/wiki/Requests#limit&quot; target&#x3D;&quot;_blank&quot;&gt;Docs&lt;/a&gt;
     */
    limit?: number;

    /**
     * Offset amount of resources. &lt;a href&#x3D;&quot;https://github.com/nestjsx/crud/wiki/Requests#offset&quot; target&#x3D;&quot;_blank&quot;&gt;Docs&lt;/a&gt;
     */
    offset?: number;

    /**
     * Page portion of resources. &lt;a href&#x3D;&quot;https://github.com/nestjsx/crud/wiki/Requests#page&quot; target&#x3D;&quot;_blank&quot;&gt;Docs&lt;/a&gt;
     */
    page?: number;

    /**
     * Reset cache (if was enabled). &lt;a href&#x3D;&quot;https://github.com/nestjsx/crud/wiki/Requests#cache&quot; target&#x3D;&quot;_blank&quot;&gt;Docs&lt;/a&gt;
     */
    cache?: number;

  }): Observable<GetManyCompanyResponseDto | Array<Company>> {

    return this.getManyBaseCompanyControllerCompany$Response(params).pipe(
      map((r: StrictHttpResponse<GetManyCompanyResponseDto | Array<Company>>) => r.body as GetManyCompanyResponseDto | Array<Company>)
    );
  }

  /**
   * Path part for operation createOneBaseCompanyControllerCompany
   */
  static readonly CreateOneBaseCompanyControllerCompanyPath = '/company';

  /**
   * Create one Company.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `createOneBaseCompanyControllerCompany()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  createOneBaseCompanyControllerCompany$Response(params: {
      body: Company
  }): Observable<StrictHttpResponse<Company>> {

    const rb = new RequestBuilder(this.rootUrl, ApiService.CreateOneBaseCompanyControllerCompanyPath, 'post');
    if (params) {


      rb.body(params.body, 'application/json');
    }
    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Company>;
      })
    );
  }

  /**
   * Create one Company.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `createOneBaseCompanyControllerCompany$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  createOneBaseCompanyControllerCompany(params: {
      body: Company
  }): Observable<Company> {

    return this.createOneBaseCompanyControllerCompany$Response(params).pipe(
      map((r: StrictHttpResponse<Company>) => r.body as Company)
    );
  }

  /**
   * Path part for operation createManyBaseCompanyControllerCompany
   */
  static readonly CreateManyBaseCompanyControllerCompanyPath = '/company/bulk';

  /**
   * Create many Company.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `createManyBaseCompanyControllerCompany()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  createManyBaseCompanyControllerCompany$Response(params: {
      body: CreateManyCompanyDto
  }): Observable<StrictHttpResponse<Array<Company>>> {

    const rb = new RequestBuilder(this.rootUrl, ApiService.CreateManyBaseCompanyControllerCompanyPath, 'post');
    if (params) {


      rb.body(params.body, 'application/json');
    }
    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Array<Company>>;
      })
    );
  }

  /**
   * Create many Company.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `createManyBaseCompanyControllerCompany$Response()` instead.
   *
   * This method sends `application/json` and handles request body of type `application/json`.
   */
  createManyBaseCompanyControllerCompany(params: {
      body: CreateManyCompanyDto
  }): Observable<Array<Company>> {

    return this.createManyBaseCompanyControllerCompany$Response(params).pipe(
      map((r: StrictHttpResponse<Array<Company>>) => r.body as Array<Company>)
    );
  }

}
