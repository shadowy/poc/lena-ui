/* tslint:disable */
import { Company } from './company';
export interface GetManyCompanyResponseDto {
  count: number;
  data: Array<Company>;
  page: number;
  pageCount: number;
  total: number;
}
