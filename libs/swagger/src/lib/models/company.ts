/* tslint:disable */
export interface Company {
  description: string;
  id: number;
  isActive: boolean;
  name: string;
}
