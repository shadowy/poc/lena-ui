/* tslint:disable */
import { Company } from './company';
export interface CreateManyCompanyDto {
  bulk: Array<Company>;
}
