import { ChangeDetectorRef, Directive, ElementRef, Host, Input, OnDestroy, OnInit, Self } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { TranslateService } from '@ngx-translate/core';
import { GridHelper, GridHelperFilter, GridHelperSettings } from '../util/grid';

@Directive({
  selector: '[utilRestGrid]'
})
export class RestGridDirective implements OnInit, OnDestroy{
  private _grid: GridHelper;
  private _filter: GridHelperFilter;
  private readonly noDataElement: HTMLElement;

  get grid(): GridHelper {
    return this._grid;
  }

  @Input() private settings: GridHelperSettings;
  @Input() private set filter(value : GridHelperFilter) {
    this._filter = value;
    if (!!this._grid) {
      this._grid.setFilterSettings(value);
    }
  }

  constructor(private element: ElementRef<HTMLElement>, private cdRef: ChangeDetectorRef,
              @Host() @Self() private table: MatTable<any>, @Host() @Self() private sort: MatSort,
              private translate: TranslateService) {
    this.noDataElement = document.createElement('div');
    this.noDataElement.className = 'grid-empty';
    this.noDataElement.textContent = this.translate.instant('COMMON.GRID.NO-DATA');
    this.noDataElement.style.display = 'none';
    if (!!this.element) {
      this.element.nativeElement.parentNode.append(this.noDataElement);
    }
  }

  ngOnInit(): void {
    if (!this.settings) {
      return;
    }
    const self = this;
    const control = {
      showNoData() {
        self.noDataElement.style.display = 'block';
      },
      hideNoData() {
        self.noDataElement.style.display = 'none';
      }
    };

    this._grid = new GridHelper(this.element.nativeElement.parentElement, this.table, this.sort, this.settings,
      this._filter, control, this.cdRef);
  }

  ngOnDestroy(): void {
    if (!!this._grid) {
      this.noDataElement.remove();
      this._grid.destroy();
    }
  }

}
