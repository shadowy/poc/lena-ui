import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RestGridDirective } from './rest-grid/rest-grid.directive';

@NgModule({
  imports: [CommonModule],
  declarations: [RestGridDirective],
  exports: [RestGridDirective]
})
export class UtilsModule {}
