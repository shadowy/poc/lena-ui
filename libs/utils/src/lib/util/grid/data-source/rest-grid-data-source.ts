import { EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';
import { GridDataSource, GridRequest } from '../grid-data-source';

export class RestGridDataSource implements GridDataSource {
  private method: (param: any) => Observable<any>;
  private filter: any;

  onDataChanged = new EventEmitter<void>();

  init(method: (param: any) => Observable<any>, data$: Observable<any[]>): GridDataSource {
    this.method = method;
    if (this.method === null) {
      console.warn('Method not found');
      return null;
    }
    return this;
  }

  setFilter(filter: any) {
    this.filter = filter;
  }

  loadData(offset: number, size: number, field: string, order: string): Promise<any[]> {
    const sort = !field ? undefined : `${field},${order.toUpperCase()}`;
    const filter = !!this.filter && !!this.filter.filter ? this.filter.filter : undefined;
    const or = !!this.filter && !!this.filter.or ? this.filter.or : undefined;
    return new Promise<any[]>((resolve, reject) => {
      this.method({
        limit: size,
        offset: offset,
        sort: sort,
        filter: filter,
        or: or
      }).subscribe(data => {
        resolve(data.data);
      }, err => reject(err));
    });
  }

  destroy() {
    this.onDataChanged.complete();
  }
}
