import { EventEmitter } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { GridDataSource } from '../grid-data-source';

export class ArrayGridDataSource implements GridDataSource{
  private data: any[] = [];
  private subscription: Subscription;

  onDataChanged = new EventEmitter<void>();

  init(method: (param: any) => Observable<any>, data$: Observable<any[]>): GridDataSource {
    if (!!this.subscription) {
      this.subscription.unsubscribe();
    }
    this.subscription = data$.subscribe(data => {
      this.data = data || [];
      this.onDataChanged.emit();
    });
    return this;
  }

  loadData(offset: number, size: number, field: string, order: string): Promise<any[]> {
    order = order === '' || order === undefined || order === null ? 'asc' : order.toLowerCase();
    if (field !== '') {
      sortObject(p => p[field], this.data, order);
    }
    return Promise.resolve(this.data.slice(offset, size));
  }

  setFilter(filter: any) {
  }

  destroy() {
    if (!!this.subscription) {
      this.subscription.unsubscribe();
      this.subscription = null;
      this.onDataChanged.complete();
    }
  }
}

function sortObject<T>(prop: (c: any) => T, list: any, order: string): void {
  list.sort((a, b) => {
    if (prop(a) < prop(b)) {
      return -1;
    }
    if (prop(a) > prop(b)) {
      return 1;
    }
    return 0;
  });

  if (order === 'desc') {
    list.reverse();
  }
}
