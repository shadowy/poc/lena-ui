import { ChangeDetectorRef, ViewRef } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { Observable, Subscription } from 'rxjs';
import { GridDataSource } from './grid-data-source';
import { ArrayGridDataSource, RestGridDataSource } from './data-source';

const epsilon = 10;

export interface GridHelperFilter {
  filter: Observable<any>;
}

export interface GridHelperControl {
  showNoData();
  hideNoData();
}

export interface GridHelperSettings {
  mode?: string;
  method: (param: any) => Observable<any>;
  data?: Observable<any[]>;
}

const dataSource = {
  'rest': () => RestGridDataSource,
  'array': () => ArrayGridDataSource
};

export class GridHelper {
  private block = false;
  private subscription;
  private dataSource: GridDataSource;
  private filterSettings: GridHelperFilter;
  private pos = {
    offset: 0,
    size: 200
  };
  private sorting = {
    field: '',
    order: null
  };

  data: any[] = [];

  constructor(private element: HTMLElement, private table: MatTable<any>, private sort: MatSort,
              private settings: GridHelperSettings, filterSettings: GridHelperFilter,
              private control: GridHelperControl, private cdRef: ChangeDetectorRef) {
    settings.mode = !settings.mode ? 'rest': settings.mode;
    const con = dataSource[settings.mode];
    if (!con) {
      console.warn('DataSource not found', this.settings);
      return null;
    }
    const constr = con();
    this.dataSource = (new constr()).init(settings.method, settings.data);
    if (!this.settings) {
      console.warn('Settings not found', this.settings);
      return;
    }
    this.table.dataSource = [];
    this.initScroll();
    this.setFilterSettings(filterSettings);
    this.dataSource.onDataChanged.subscribe(() => this.refresh());
  }

  refresh() {
    this.pos.offset = 0;
    this.data = [];
    this.table.dataSource = this.data;
    this.markForCheck();
    this.loadData();
    this.block = true;
  }

  setFilterSettings(filter: GridHelperFilter) {
    this.filterSettings = filter;
    if (!!this.subscription) {
      this.subscription.unsubscribe();
    }
    this.subscription = new Subscription();
    this.initSort();
    this.initFilter();
  }

  setFilter(data: any) {
    this.dataSource.setFilter(data);
    this.refresh();
  }

  setSort(field: string, order: string) {
    if (order === '') {
      this.sorting.field = '';
      this.sorting.order = null;
    } else {
      this.sorting.field = field;
      this.sorting.order = order;
    }
    this.refresh();
  }

  destroy() {
    this.dataSource.destroy();
    this.subscription.unsubscribe();
  }

  private loadData() {
    this.dataSource.loadData(this.pos.offset, this.pos.size, this.sorting.field, this.sorting.order)
      .then(data => {
        this.data = [...this.data, ...data];
        this.table.dataSource = this.data;
        if (this.data.length > 0) {
          this.control.hideNoData();
        } else {
          this.control.showNoData();
        }
        this.pos.offset = this.data.length;
        this.markForCheck();
        this.block = false;
      });
  }

  private markForCheck() {
    if (!(this.cdRef as ViewRef).destroyed) {
      this.cdRef.markForCheck();
    }
  }

  private initScroll() {
    this.element.addEventListener('scroll', (event: Event) => {
      const height = event.target['clientHeight'];
      const top = event.target['scrollTop'];
      const fullHeight = event.target['scrollHeight'];
      if (top + height >= fullHeight - epsilon) {
        if (top !== 0) {
          this.loadData();
        }
      }
    });
  }

  private initSort() {
    this.subscription.add(this.sort.sortChange.subscribe(result => {
      this.setSort(result.active, result.direction);
    }));
  }

  private initFilter() {
    if (!this.filterSettings) {
      return;
    }
    this.subscription.add(this.filterSettings.filter.subscribe(data => {
      this.setFilter(data);
    }));
  }
}
