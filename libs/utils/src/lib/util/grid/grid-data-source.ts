import { EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';

export interface GridRequest {
  setGrid(data: any);
  setSearch(data: any);
}

export interface GridDataSource {
  onDataChanged: EventEmitter<void>;
  init(method: (param: any) => Observable<any>, data$: Observable<any[]>): GridDataSource;
  setFilter(filter: any);
  loadData(offset: number, size: number, field: string, order: string): Promise<any[]>;
  destroy();
}
