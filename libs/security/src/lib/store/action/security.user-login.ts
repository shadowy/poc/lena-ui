export class SecurityUserLogin {
  static readonly type = '[Security] User login';

  constructor(public payload: any) {}
}
