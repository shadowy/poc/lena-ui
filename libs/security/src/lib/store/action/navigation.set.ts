export class NavigationSet {
  static readonly type = '[Navigation] Set';

  constructor(public payload: string) {}
}
