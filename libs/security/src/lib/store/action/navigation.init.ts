import { NavigationItem } from '../navigation.state.model';

export class NavigationInit {
  static readonly type = '[Navigation] Init';

  constructor(public payload: {list: NavigationItem[], code: string}) {}
}
