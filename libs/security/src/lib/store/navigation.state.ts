import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext, Store } from '@ngxs/store';
import { NavigationInit, NavigationSet } from './action';
import { defaultNavigationState, NavigationItem, NavigationStateModel } from './navigation.state.model';

@State<NavigationStateModel>(defaultNavigationState)
@Injectable()
export class NavigationState {
  @Selector() static getList(state: NavigationStateModel): NavigationItem[] {
    return state.list;
  }

  @Selector() static title(state: NavigationStateModel): string {
    return state.title;
  }

  constructor(private store: Store) {}

  @Action(NavigationInit) init({getState, patchState}: StateContext<NavigationStateModel>, {payload}: NavigationInit) {
    const state = getState();
    if (!state.defined[payload.code]) {
      const d = {...state.defined};
      d[payload.code] = payload.list;
      return patchState({defined: d});
    }
  }

  @Action(NavigationSet) Set({getState, patchState}: StateContext<NavigationStateModel>, {payload}: NavigationSet) {
    const state = getState();
    const d = state.defined[payload];
    return patchState({list: !!d ? d : [], title : ''});
  }
}

