export interface NavigationLayout {
  showNavigation: boolean;
}

export interface UserInfo {
  id: string;
  email: string;
  firstName: string;
  lastName: string;
  patronymicName: string;
  photo: string;
  token: string;
}

export interface SecurityStateModel {
  user: UserInfo;
  page: NavigationLayout;
  version: string;
}

export const defaultSecurityState = {
  name: 'security',
  defaults: {
    user: null,
    page: {
      showNavigation: false
    },
    version: 'dev'
  }
};
