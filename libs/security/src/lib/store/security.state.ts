import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { Action, NgxsOnInit, Selector, State, StateContext, Store } from '@ngxs/store';
import { StorageService } from '../service';
import { SecurityUserLogin, SecurityUserLogout, SecurityVersion } from './action';
import { defaultSecurityState, NavigationLayout, SecurityStateModel, UserInfo } from './security.state.model';

@State<SecurityStateModel>(defaultSecurityState)
@Injectable()
export class SecurityState implements NgxsOnInit {
  static isInitiated = false;

  @Selector()
  static User(state: SecurityStateModel): UserInfo {
    return state.user;
  }

  @Selector()
  static CurrentUserInfo(state: SecurityStateModel): UserInfo {
    return state.user;
  }

  @Selector()
  static Navigation(state: SecurityStateModel): NavigationLayout {
    return state.page;
  }

  @Selector()
  static Version(state: SecurityStateModel): string {
    return state.version;
  }

  constructor(private store: Store, private router: Router, private auth: AngularFireAuth, storage: StorageService) {
    const logout = () => {
      SecurityState.isInitiated = true;
      return this.userIsLogout();
    }
    this.auth.user.subscribe(data => {
      const signIn = signInWithLink(storage, this.auth);
      if (!!signIn) {
        return signIn.then(() => getToken(this.auth).then(user => this.userIsLogin(user))).catch(() => logout());
      }
      if (data === null) {
        return logout();
      }
      getToken(this.auth).then(user => this.userIsLogin(user)).catch(() => logout());
    });
  }

  ngxsOnInit(ctx: StateContext<SecurityStateModel>) {
    ctx.dispatch(new SecurityVersion());
  }

  @Action(SecurityUserLogin) login({patchState, dispatch}: StateContext<SecurityStateModel>, {payload}: SecurityUserLogin) {
    if (!SecurityState.isInitiated) {
      SecurityState.isInitiated = true;
    }
    return patchState({
      user: {
        id: payload.uid,
        firstName: payload.firstname,
        lastName: payload.lastname,
        patronymicName: payload.patronymictname,
        email: payload.email,
        photo: payload.photoURL || '',
        token: payload.token
      },
      page: {
        showNavigation: true
      }
    });
  }

  @Action(SecurityUserLogout) logout({patchState}: StateContext<SecurityStateModel>) {
    return patchState({
      user: null,
      page: {
        showNavigation: false
      }
    });
  }

  @Action(SecurityVersion) version({patchState}: StateContext<SecurityStateModel>) {
    return new Promise((resolve => {
      const xhr = new XMLHttpRequest();
      xhr.open('GET', '/assets/version.txt', true);
      xhr.send();
      xhr.onreadystatechange = () => {
        if (xhr.status === 200) {
          resolve(patchState({version: xhr.responseText.replace(/\n/g, '')}));
        } else {
          resolve();
        }
      };
    }));
  }

  private userIsLogin(data: any) {
    this.store.dispatch(new SecurityUserLogin(data));
  }

  private userIsLogout() {
    this.store.dispatch(new SecurityUserLogout());
    return this.router.navigate(['/login']);
  }
}

function signInWithLink(storage: StorageService, auth: AngularFireAuth): Promise<any> {
  const email = storage.getData().email;
  if (!email) {
    return null;
  }
  return auth.signInWithEmailLink(email, window.location.href).then(result => {
    storage.setData({});
    return result;
  }).catch(error => {
    console.error(error);
    return null;
  });
}

function getToken(auth: AngularFireAuth): Promise<any> {
  return auth.currentUser.then(user => user.getIdToken().then(token => ({token, ...user})));
}
