export interface NavigationItem {
  path: string;
  icon: string;
  title: string;
  permissions: string[];
}

export interface NavigationStateModel {
  defined: {string: NavigationItem[]},
  title: string;
  list: NavigationItem[];
}

export const defaultNavigationState = {
  name: 'navigation',
  defaults: {
    title: '',
    defined: {} as {string: NavigationItem[]},
    list: []
  }
};
