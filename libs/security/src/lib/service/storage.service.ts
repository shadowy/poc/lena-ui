import { Injectable } from '@angular/core';

const path = 'smart-sales';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor() { }

  getData(): any {
    return JSON.parse(window.localStorage.getItem(path) || '{}');
  }

  setData(data: any) {
    window.localStorage.setItem(path, JSON.stringify(data || {}));
  }

  patchData(data: any): any {
    this.setData({...this.getData(), ...data});
  }

  getLanguage() {
    return window.localStorage.getItem(path + ':lang');
  }

  setLanguage(lang: string) {
    window.localStorage.setItem(path + ':lang', lang);
  }
}
