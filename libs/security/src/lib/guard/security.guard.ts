import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Actions, ofActionCompleted, Store } from '@ngxs/store';
import { SecurityState, SecurityUserLogin } from '../store';

@Injectable({
  providedIn: 'root'
})
export class SecurityGuard implements CanActivate {
  constructor(protected store: Store, private actions: Actions, private router: Router) {}

  canActivate( next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Promise<boolean | UrlTree> {
    return new Promise(((resolve) => {
      if (SecurityState.isInitiated) {
        return resolve(this.checkUser(next))
      }
      const subscription = this.actions.pipe(ofActionCompleted(SecurityUserLogin)).subscribe(() => {
        setTimeout(() => subscription.unsubscribe());
        return resolve(this.checkUser(next));
      });
    }));
  }

  private checkUser(next: ActivatedRouteSnapshot): boolean | UrlTree {
    const user = this.store.selectSnapshot(store => store.security.user);
    if (next.data.isLogin) {
      if (!user) {
        return true;
      }
      return this.router.parseUrl('/');
    }
    if (!user) {
      return this.router.parseUrl('/login');
    }
    return true;
  }
}
