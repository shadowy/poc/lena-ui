import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { NavigationSet } from '../store/action';
import { SecurityGuard } from './security.guard';

@Injectable({
  providedIn: 'root'
})
export class NavigationGuard extends SecurityGuard {
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Promise<boolean | UrlTree> {
    return super.canActivate(next, state).then((data: boolean | UrlTree) => {
      if (typeof data === 'boolean') {
        if (data) {
          this.store.dispatch(new NavigationSet(next.data.navigation));
        }
      }
      return data;
    });
  }
}
