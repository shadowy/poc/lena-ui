import { TestBed, async, inject } from '@angular/core/testing';
import { RouterModule } from '@angular/router';
import { NgxsModule } from '@ngxs/store';

import { NavigationGuard } from './navigation.guard';

describe('NavigationGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [NgxsModule.forRoot([]), RouterModule.forRoot([], {useHash: true})],
      providers: [NavigationGuard]
    });
  });

  it('should ...', inject([NavigationGuard], (guard: NavigationGuard) => {
    expect(guard).toBeTruthy();
  }));
});
