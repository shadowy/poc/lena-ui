import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxsModule } from '@ngxs/store';
import { NavigationState, SecurityState } from './store';


@NgModule({
  imports: [CommonModule, NgxsModule.forFeature([SecurityState, NavigationState])]
})
export class SecurityModule { }
