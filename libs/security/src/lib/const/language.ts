export const flagLang = {'en': 'gb', 'uk': 'ua'};
export const languages = Object.keys(flagLang);
export const defaultLang = languages[0];
