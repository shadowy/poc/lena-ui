import { Component } from '@angular/core';

@Component({
  selector: 'smartsales-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  title = 'smart-sales';
}
