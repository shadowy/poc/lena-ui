import { Routes } from '@angular/router';
import { NavigationGuard } from '@smartsales/security';
import { ListComponent } from './list/list.component';

export const routes: Routes = [
  {
    path: '',
    component: ListComponent,
    canActivate: [NavigationGuard],
    data: {
      navigation: 'company'
    }
  }
];
