import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '@smartsales/swagger';
import { GridHelperSettings } from '@smartsales/utils';
import { Observable } from 'rxjs';

@Component({
  selector: 'smartsales-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListComponent {
  displayedColumns: string[] = ['name', 'description', 'isActive'];
  settings: GridHelperSettings = {method: (param) => this.getData(param)};

  constructor(private router: Router, private activeRoute: ActivatedRoute, private api: ApiService) {}

  open(row: any) {
    return this.router.navigate(['..', row.id], {relativeTo: this.activeRoute});
  }

  private getData(param: any): Observable<any> {
    return this.api.getManyBaseCompanyControllerCompany(param);
  }
}
