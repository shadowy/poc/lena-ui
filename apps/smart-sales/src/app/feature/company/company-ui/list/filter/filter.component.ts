import { ChangeDetectionStrategy, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatInput } from '@angular/material/input';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'smartsales-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FilterComponent implements OnInit, OnDestroy{
  @ViewChild(MatInput) text: MatInput;
  filter: BehaviorSubject<any> = new BehaviorSubject({});

  search() {
    const filter = {};
    if (!!this.text && !!this.text.value) {
      filter['filter'] = 'name||$starts||' + this.text.value
      filter['or'] = ['description||$starts||' + this.text.value];
    }
    this.filter.next(filter);
  }

  ngOnInit() {
    this.search();
  }

  ngOnDestroy() {
    this.filter.complete();
  }
}
