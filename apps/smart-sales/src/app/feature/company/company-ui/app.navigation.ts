import { NavigationItem } from '@smartsales/security';

export const navigation: NavigationItem[] = [
  {
    title: 'NAVIGATION.COMPANY.LIST',
    path: '/company/',
    icon: 'business',
    permissions: []
  }
];
