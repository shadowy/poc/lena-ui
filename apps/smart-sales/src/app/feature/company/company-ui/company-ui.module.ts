import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { NgxsModule, Store } from '@ngxs/store';
import { NavigationInit } from '@smartsales/security';
import { UtilsModule } from '@smartsales/utils';
import { navigation } from './app.navigation';
import { routes } from './app.routes';
import { ListComponent } from './list/list.component';
import { FilterComponent } from './list/filter/filter.component';


@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes), NgxsModule, TranslateModule, MatTableModule, MatSortModule,
    MatIconModule, MatFormFieldModule, MatInputModule, MatButtonModule, UtilsModule],
  declarations: [ListComponent, FilterComponent]
})
export class CompanyUiModule {
  constructor(store: Store) {
    store.dispatch(new NavigationInit({list: navigation, code: 'company'}));
  }
}
