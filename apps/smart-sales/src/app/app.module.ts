import { HTTP_INTERCEPTORS, HttpClient, HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { NgxsLoggerPluginModule } from '@ngxs/logger-plugin';
import { NgxsModule } from '@ngxs/store';
import { LocaleService, NgLocaleModule } from '@shadowy/ng-locale';
import { defaultLang, languages, SecurityInterceptor, SecurityModule, StorageService } from '@smartsales/security';
import { SecurityUIModule } from '@smartsales/security-ui';
import { ApiModule } from '@smartsales/swagger';
import { UtilsModule } from '@smartsales/utils';
import { NgxAuthFirebaseUIModule } from 'ngx-auth-firebaseui';
import { FIREBASE_OPTIONS } from '@angular/fire';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { routes } from './app.routes';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, 'assets/i18n/application/');
}

export function firebaseOption() {
  return (window as any).settings.firebase;
}

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, BrowserAnimationsModule, SecurityModule, SecurityUIModule, HttpClientModule,
    NgxsModule.forRoot(), RouterModule.forRoot(routes), NgLocaleModule, ApiModule.forRoot({rootUrl: '/api'}),
    NgxsReduxDevtoolsPluginModule.forRoot({disabled: (window as any).settings.production}),
    NgxsLoggerPluginModule.forRoot({disabled: (window as any).settings.production}),
    NgxAuthFirebaseUIModule.forRoot(undefined, undefined, {
      authGuardFallbackURL: '/security/login',
      authGuardLoggedInURL: '/',
      guardProtectedRoutesUntilEmailIsVerified: false
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })],
  providers: [
    {
      provide: FIREBASE_OPTIONS,
      useFactory: firebaseOption

    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: SecurityInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(translate: TranslateService, locale: LocaleService, storage: StorageService) {
    translate.addLangs(languages);
    const lang = storage.getLanguage();
    if (!!lang) {
      translate.use(lang);
    } else if (languages.indexOf(translate.getBrowserLang()) >= 0) {
      translate.use(translate.getBrowserLang());
    } else {
      translate.use(defaultLang);
    }
    locale.set('uk-UA');
  }
}
