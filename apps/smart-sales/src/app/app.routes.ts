import { Routes } from '@angular/router';
import { DefaultPageComponent, LoginPageComponent } from '@smartsales/security-ui';
import { SecurityGuard } from '@smartsales/security';

export const routes: Routes = [
  {
    path: 'login',
    component: LoginPageComponent,
    canActivate: [SecurityGuard],
    data: {
      isLogin: true
    }
  },
  {
    path: 'company',
    loadChildren: () => import('./feature/company/company-ui/company-ui.module').then(m => m.CompanyUiModule)
  },
  {
    path: '**',
    component: DefaultPageComponent,
    canActivate: [SecurityGuard]
  }
];
